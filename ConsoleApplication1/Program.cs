﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Newtonsoft.Json;
namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            string path = @"D:\test\";

            DirectoryInfo dir = new DirectoryInfo(path);

            FileInfo[] infos = dir.GetFiles("*.*", SearchOption.AllDirectories);

            Dictionary<string, long[]> fileInfos = new Dictionary<string, long[]>();
            foreach (FileInfo info in infos)
            {
                if (!fileInfos.ContainsKey(info.Extension))
                {
                    fileInfos.Add(info.Extension, new long[] { 1, info.Length, info.Length });
                }
                else
                {
                   long[] tmpVal = fileInfos[info.Extension];
                   tmpVal[0]++; //incrment count
                   // process fi
                   long thisFileLength = info.Length;
                   if (thisFileLength < tmpVal[1])
                   {
                       tmpVal[1] = thisFileLength;
                   }
                   else if (thisFileLength > tmpVal[2])
                   {
                       tmpVal[2] = thisFileLength;
                   }
                   fileInfos[info.Extension] = tmpVal;
                }
            }

            
            Console.WriteLine("Result");
            foreach (KeyValuePair<String, long[]> item in fileInfos)
            {
                Console.WriteLine("Ext: {0} \n\tCount: {1}\n\tMinLenght: {2},\n\tMaxLen: {3}",
                    item.Key, item.Value[0], item.Value[1], item.Value[2]);
                Console.WriteLine("----------------------------------------------------------------------------");
            }

            //SaveResults(fileInfos, path);

            Console.WriteLine("----------------------------------------------------------------------------");

            Dictionary<string, long[]> newData = ReadData(@"D:\test\27.08.2016_log.txt");

            Console.WriteLine("Result from read");
            foreach (KeyValuePair<String, long[]> item in fileInfos)
            {
                Console.WriteLine("Ext: {0} \n\tCount: {1}\n\tMinLenght: {2},\n\tMaxLen: {3}",
                    item.Key, item.Value[0], item.Value[1], item.Value[2]);
                Console.WriteLine("----------------------------------------------------------------------------");
            }
             */

            DirectoryInfo dir_info = new DirectoryInfo("D:\\");
            List<string> file_list = new List<string>();
            SearchDirectory(dir_info, file_list);
            foreach(string f in file_list)
                Console.WriteLine(f);

        }

        static void SearchDirectory(DirectoryInfo dir_info, List<string> file_list)
        {
            try
            {
                foreach (DirectoryInfo subdir_info in dir_info.GetDirectories())
                {
                    Console.WriteLine("dir");
                    SearchDirectory(subdir_info, file_list);
                }
            }
            catch
            {
                Console.WriteLine("Ex");
            }
            try
            {
                foreach (FileInfo file_info in dir_info.GetFiles())
                {
                    file_list.Add(file_info.FullName);
                }
            }
            catch
            {
                Console.WriteLine("Ex");
            }
        }

        private static void SaveResults(Dictionary<string, long[]> data, string path)
        {
            DateTime now = DateTime.Now;

            string json = JsonConvert.SerializeObject(data, Newtonsoft.Json.Formatting.Indented);

            string filePath = path + now.ToShortDateString() + @"_log.txt";
            using (StreamWriter sw = File.CreateText(filePath))
            {
                sw.Write(json);
            }

        }

        private static Dictionary<string, long[]> ReadData(string filename)
        {
            string jsonString = File.ReadAllText(filename, Encoding.Default);
            return JsonConvert.DeserializeObject<Dictionary<string, long[]>>(jsonString);
        }
    }
}
