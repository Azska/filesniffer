﻿namespace FileSniffer.Model
{
    public class ExtentionInfo
    {
        public double MinFileSize { get; set; }
        public double MaxFileSize { get; set; }
        public double TotalFilesSize { get; set; }
        public long FilesCount { get; set; }
        public string MinFileName { get; set; }
        public string MaxFileName { get; set; }

    }
}
