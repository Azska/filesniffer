﻿using System;
using System.Collections.Generic;

namespace FileSniffer.Model
{
    public class SerializationExtentionInfo
    {
        public string FolderPath { get; set; }
        public DateTime Data { get; set; }
        public Dictionary<string, ExtentionInfo> FileInfos{ get; set; }
    }
}
