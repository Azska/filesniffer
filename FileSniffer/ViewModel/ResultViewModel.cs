﻿using FileSniffer.Model;
using FileSniffer.Unitls;
using OxyPlot;
using OxyPlot.Series;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace FileSniffer.ViewModel
{
    public class ResultViewModelView : INotifyPropertyChanged
    {
        #region propperties
        private Dictionary<string, ExtentionInfo> data;
        public Dictionary<string, ExtentionInfo> Data
        {
            get
            {
                return data;
            }
            set
            {
                if (value != null)
                {
                    data = value;
                    RaisePropertyChanged("Data");
                }
            }
        }

        public PlotModel Model { get; set; }

        private bool _isVisible;
        public bool isVisible {
            get
            { 
                return _isVisible; 
            }
            set {
                _isVisible = true;
                RaisePropertyChanged("isVisible");
            }
        }

        private string info;
        public string Info
        {
            get
            {
                return info;
            }
            set
            {
                if (value != null)
                {
                    info = value;
                    RaisePropertyChanged("Info");
                };
            }
        }
        #endregion

        public CustomCommand DrawByCount { get; set; }
        public CustomCommand DrawBySize { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;


        public ResultViewModelView()
        {
            DataFileUtility util = new DataFileUtility();
            SerializationExtentionInfo tmp = new SerializationExtentionInfo();

            LoadCommands();
            tmp = util.TryLoadSerializedData();
            if (tmp != null)
            {
                isVisible = true;
                Data = tmp.FileInfos;
                //defoult choise
                BuildChartByCount();
                RaisePropertyChanged("Model");

                Info = "Данные для: " + tmp.FolderPath + " на " + tmp.Data.ToString("dd/MM/yyyy H:mm");
            }
            else
            {
                isVisible = false;
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void LoadCommands()
        {
            DrawByCount = new CustomCommand(DrawCountPieChart, canDraw);
            DrawBySize = new CustomCommand(DrawSizePieChart, canDraw);
        }

        private void BuildChartByCount()
        {
            try
            {
                Model = new PlotModel();
                PieSeries series = new PieSeries { StartAngle = 0, AngleSpan = 360 };
                long totalDataCount = Data.Values.Sum(x => x.FilesCount);
                double tmpValues = 0.0;
                string other = "";
                foreach (var d in Data)
                {
                    if ((d.Value.FilesCount * 100) / totalDataCount > 1.0)
                        series.Slices.Add(new PieSlice(d.Key, d.Value.FilesCount));
                    else if (Data.Keys.Count < 10)
                        series.Slices.Add(new PieSlice(d.Key, d.Value.FilesCount));
                    else
                    {
                        tmpValues += d.Value.FilesCount;
                        other += d.Key + ",";
                    }
                }
                if (tmpValues != 0.0)
                {
                    series.Slices.Add(new PieSlice("Other", tmpValues));
                }
                Model.Series.Add(series);
            }
            catch { }
        }

        private void BuildChartBySize()
        {
            try
            {
                Model = new PlotModel();
                PieSeries series = new PieSeries { StartAngle = 0, AngleSpan = 360 };

                double totalDataSize = Data.Values.Sum(x => x.TotalFilesSize);
                double tmpValues = 0.0;
                string other = "";

                foreach (var d in Data)
                {
                    if ((d.Value.TotalFilesSize * 100) / totalDataSize > 1.0)
                        series.Slices.Add(new PieSlice(d.Key, d.Value.TotalFilesSize));
                    else if (Data.Keys.Count < 10)
                        series.Slices.Add(new PieSlice(d.Key, d.Value.TotalFilesSize));
                    else
                    {
                        tmpValues += d.Value.TotalFilesSize;
                        other += d.Key + ",";
                    }
                }
                if (tmpValues != 0.0)
                {
                    series.Slices.Add(new PieSlice("Other", tmpValues));
                }
                Model.Series.Add(series);
            }
            // soo bad
            catch
            { }
        }

        #region command processing
        private void DrawCountPieChart(object obj)
        {
            BuildChartByCount();
            RaisePropertyChanged("Model");
        }

        private void DrawSizePieChart(object obj)
        {
            BuildChartBySize();
            RaisePropertyChanged("Model");
        }

        private bool canDraw(object obj)
        {
            return true;
        }
        #endregion

    }
}
