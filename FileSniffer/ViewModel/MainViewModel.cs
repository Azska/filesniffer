﻿using FileSniffer.DataAccess;
using FileSniffer.Model;
using FileSniffer.Unitls;
using FileSniffer.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FileSniffer.ViewModel
{
    class MainViewModel : INotifyPropertyChanged
    {
        public ICommand SetPathCommand { get; set; }
        public ICommand ResultCommand { get; set; }
        public ICommand StartProcessing { get; set; }
        public ICommand CancelProgessing { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        #region properties

        private double progressValue;
        public double ProgressValue
        {
            get
            {
                return progressValue;
            }
            set
            {

                    progressValue = value;
                    RaisePropertyChanged("ProgressValue");
            }
        }

        private string progressState;
        public string ProgressState
        {
            get
            {
                return progressState;
            }
            set
            {
                if (value != null)
                {
                    progressState = value;
                    RaisePropertyChanged("ProgressState");
                }
            }
        }

        private string path;
        public string Path
        {
            get
            {
                return path;
            }
            set
            {
                if (value != null)
                {
                    path = value;
                    RaisePropertyChanged("Path");
                }
            }
        }

        private bool canShowResult;
        public bool CanShowResult
        {
            get
            {
                return canShowResult;
            }

            set
            {
                canShowResult = value;
                RaisePropertyChanged("CanShowResult");
            }
        }

        private string processingButtonContent;
        public string ProcessingButtonContent
        {
            get
            {
                return processingButtonContent;
            }
            set
            {
                if (value != null)
                {
                    processingButtonContent = value;
                    RaisePropertyChanged("ProcessingButtonCintent");
                }
            }
        }

        private CancellationTokenSource _cts;

        #endregion

        public MainViewModel()
        {
            DataFileUtility util = new DataFileUtility();
            if (util.HavePrevData())
                CanShowResult = true;
            else CanShowResult = false;
            LoadCommands();
            Path = "";
            ProcessingButtonContent = "Начать";

        }

        private void LoadCommands()
        {
            SetPathCommand = new CustomCommand(SetPath, CanSetPath);
            ResultCommand = new CustomCommand(ShowResultView, CanShowResultView);
            StartProcessing = new CustomCommand(StartDataProcessing, CanProcess);
            CancelProgessing = new CustomCommand(CancelProcess, CanCancel);
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UpdateProgressBar(double value)
        {
            ProgressValue = value;
        }

        private void UpdateProgressText(string state)
        {
            ProgressState = state;
        }

        #region Command processing

        private void SetPath(object obj)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                Path = dialog.SelectedPath;
            }

        }

        private bool CanSetPath(object obj)
        {
            return true;
        }

        private void ShowResultView(object obj)
        {
            ResultView resultView = new ResultView();
            resultView.ShowDialog();
        }

        private bool CanShowResultView(object obj)
        {
            return CanShowResult;
        }

        private bool CanProcess(object obj)
        {
            if (!String.IsNullOrEmpty(path))
                return true;
            return false;
        }

        private async void StartDataProcessing(object obj)
        {
            _cts = new CancellationTokenSource();
            CancellationToken token = _cts.Token;
            CanShowResult = false;
            ProgressValue = 0;

            Dictionary<string, ExtentionInfo> data = null;
            FileSnifferCoreAsync core = new FileSnifferCoreAsync(path);
            Progress<double> progress = new Progress<double>(UpdateProgressBar);
            Progress<string> state = new Progress<string>(UpdateProgressText);
            try
            {
                data = await core.ProcessData(progress, state, token);

                if (data != null)
                {
                    DataFileUtility util = new DataFileUtility();
                    util.SaveData(data, path);
                    CanShowResult = true;
                    _cts = null;
                }
            }
            catch (OperationCanceledException)
            {
                UpdateProgressText("Отменено");
            }



        }

        private bool CanCancel(object obj)
        {
            if (_cts != null)
                return true;
            return false;
        }

        private void CancelProcess(object obj)
        {
            _cts.Cancel();
        }
        #endregion


    }
}
