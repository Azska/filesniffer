﻿using FileSniffer.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FileSniffer.Unitls
{
    public class DataFileUtility
    {
        private const string FILENAMETEMPLATE = @"\d\d\.\d\d\.\d{4}_log\.txt";

        public bool HavePrevData()
        {

            List<FileInfo> infos = ReadTree();
            if (infos == null)
                return false;
            else
            {
                return true;
            }
        }

        private List<FileInfo> ReadTree()
        { 
            string path = Directory.GetCurrentDirectory();
            DirectoryInfo workingDirectoryInfo = new DirectoryInfo(path);
            FileInfo[] fileInfos = workingDirectoryInfo.GetFiles("*.txt", SearchOption.AllDirectories);
            
            Regex regex = new Regex(FILENAMETEMPLATE);
            List<FileInfo> logFiles = new  List<FileInfo>();
            foreach (FileInfo info in fileInfos)
            { 
                if (regex.IsMatch(info.Name))
                    logFiles.Add(info);
            }
            if (logFiles.Count > 0)
                return logFiles;
            else return null;
        }

        public SerializationExtentionInfo TryLoadSerializedData()
        {
            List<FileInfo> logFiles = ReadTree();
            if (logFiles != null)
                return ParseSerializedData(logFiles.OrderByDescending(x => x.CreationTime).ToList().First().FullName);
            else
                return null;

        }

        private Dictionary<string, ExtentionInfo> ParseData(string file)
        {
            SerializationExtentionInfo tmp = ParseSerializedData(file);
            return tmp.FileInfos;
        }

        private SerializationExtentionInfo ParseSerializedData(string file)
        {
            string jsonString = File.ReadAllText(file, Encoding.Default);
            return JsonConvert.DeserializeObject<SerializationExtentionInfo>(jsonString);
        }

        public void SaveData(Dictionary<string, ExtentionInfo> data, string path)
        { 
 
            DateTime now = DateTime.Now;
            SerializationExtentionInfo info = new SerializationExtentionInfo()
            {
                Data = now,
                FileInfos = data,
                FolderPath = path
            };
            string json = JsonConvert.SerializeObject(info, Newtonsoft.Json.Formatting.Indented);

            string filePath = Directory.GetCurrentDirectory() + @"\" + now.ToShortDateString() + @"_log.txt";
            using (StreamWriter streamWriter = File.CreateText(filePath))
            {
                streamWriter.Write(json);
            }
        }

    }
}
