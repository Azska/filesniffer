﻿using FileSniffer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace FileSniffer.DataAccess
{
    public class FileSnifferCore
    {
        private string path { get; set; }
        public Dictionary<string, ExtentionInfo> data = null;

        public FileSnifferCore(string path)
        {
            this.path = path;
        }

        private FileInfo[] GetFileInfos(string path)
        {
            DirectoryInfo dir_info = new DirectoryInfo(path);
            List<FileInfo> file_list = new List<FileInfo>();
            SearchDirectory(dir_info, file_list);
            return file_list.ToArray();
        }

        static void SearchDirectory(DirectoryInfo dir_info, List<FileInfo> file_list)
        {
            try
            {
                foreach (DirectoryInfo subdir_info in dir_info.GetDirectories())
                {
                    SearchDirectory(subdir_info, file_list);
                }
            }
            catch
            {
            }
            try
            {
                foreach (FileInfo file_info in dir_info.GetFiles())
                {
                    file_list.Add(file_info);
                }
            }
            catch
            {
            }
        }

        public  Task<Dictionary<string, ExtentionInfo>> Process(IProgress<double> progress)
        {
            return Task.Run( () =>
                {

            FileInfo[] infos = GetFileInfos(path);
            data = new Dictionary<string, ExtentionInfo>();
            int totalCount = infos.Length;
            DateTime reportTime = DateTime.Now;

            for (int i = 0; i < infos.Length; i++)
            {
                DateTime time = DateTime.Now;
                if (!data.ContainsKey(infos[i].Extension))
                {
                    data.Add(infos[i].Extension, new ExtentionInfo()
                    {
                        FilesCount = 1,
                        MaxFileName = infos[i].Name,
                        MaxFileSize = (double)infos[i].Length / 1024,
                        MinFileName = infos[i].Name,
                        MinFileSize = (double) infos[i].Length / 1024,
                        TotalFilesSize = (double) infos[i].Length / 1024
                    });
                }
                else
                {
                    ExtentionInfo tmpVal = data[infos[i].Extension];
                    tmpVal.FilesCount++;
                    double thisFileLength = (double)infos[i].Length / 1024;
                    tmpVal.TotalFilesSize += thisFileLength;
                    if (thisFileLength < tmpVal.MinFileSize)
                    {
                        tmpVal.MinFileSize = thisFileLength;
                        tmpVal.MinFileName = infos[i].Name;
                    }
                    else if (thisFileLength > tmpVal.MaxFileSize)
                    {
                        tmpVal.MaxFileSize = thisFileLength;
                        tmpVal.MaxFileName = infos[i].Name;
                    }
                    data[infos[i].Extension] = tmpVal;
                }
                TimeSpan diff = time.Subtract(reportTime);
                // 10k - устнановлено опытным путём
                    if ( diff.Ticks > 10000)
                    {
                        progress.Report((100 * i) / totalCount);
                        reportTime = time;
                    }
            }
            progress.Report(100);
            return data;
                });

        }


        
    }
}
